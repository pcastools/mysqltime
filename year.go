// Year contains functions and types to simplify working with MySQL YEARs. Years are location independent.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mysqltime

import (
	"database/sql/driver"
	"strconv"
	"strings"
	"time"
)

// Year represents a year. Years are location independent.
type Year int

/////////////////////////////////////////////////////////////////////////
// Year functions
/////////////////////////////////////////////////////////////////////////

// String returns the Year in the format "YYYY".
func (y Year) String() string {
	if y.IsZero() {
		return ZeroYear
	}
	return strconv.Itoa(int(y))
}

// Value returns the Year as a driver.Value, satisfying the driver.Valuer interface.
func (y Year) Value() (driver.Value, error) {
	return y.String(), nil
}

// Scan assigns a value to this Year from a database driver. The src value should be one of the types defined in the sql.Scanner interface.
func (y *Year) Scan(src interface{}) (err error) {
	switch val := src.(type) {
	case nil:
	case int64:
		if val < 1901 || val > 2155 {
			err = ErrYearRange
		} else {
			*y, err = NewYear(int(val))
		}
	case time.Time:
		*y, err = NewYear(val.Year())
	case string:
		*y, err = ParseYear(val)
	case []byte:
		*y, err = ParseYear(string(val))
	default:
		err = ErrYearScan
	}
	return
}

// After returns true iff the year y is after u.
func (y Year) After(u Year) bool {
	return y.Year() > u.Year()
}

// Before returns true iff the year y is before u.
func (y Year) Before(u Year) bool {
	return y.Year() < u.Year()
}

// Equal returns true iff the year y is equal to u.
func (y Year) Equal(u Year) bool {
	return y.Year() == u.Year()
}

// Year returns the year.
func (y Year) Year() int {
	if y.IsZero() {
		return 0
	}
	return int(y)
}

// IsZero return true if the year is outside the supported range.
func (y Year) IsZero() bool {
	year := int(y)
	return year < 1901 || year > 2155
}

// Time converts the Year to time.Time using the given location.
func (y Year) Time(loc *time.Location) (t time.Time) {
	if y.IsZero() {
		return
	}
	t = time.Date(y.Year(), time.January, 1, 0, 0, 0, 0, loc)
	return
}

// Local converts the Year to time.Time using Local location
func (y Year) Local() time.Time {
	return y.Time(time.Local)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewYear creates a new year. The year must lie in the range 1901 to 2155, inclusive, or be 0.
func NewYear(year int) (y Year, err error) {
	// Get the zero year out of the way
	if year == 0 {
		return
	}
	// Sanity check on the year
	if year < 1901 || year > 2155 {
		err = ErrYearRange
		return
	}
	// Create the year
	y = Year(year)
	return
}

// ParseYear converts the string to a Year. Leading and trailing spaces are ignored.
func ParseYear(value string) (y Year, err error) {
	// Strip off any white space
	value = strings.TrimSpace(value)
	// Special-case for a zero year
	if value == ZeroYear {
		return
	}
	// How we proceed depends on the length of the string
	var year int
	N := len(value)
	if N == 1 || N == 2 {
		if year, err = strconv.Atoi(value); err != nil {
			err = ErrYearParse
			return
		}
		if year < 0 {
			err = ErrYearParse
			return
		}
		if year < 70 {
			year += 2000
		} else {
			year += 1900
		}
	} else if N == 4 {
		if year, err = strconv.Atoi(value); err != nil {
			err = ErrYearParse
			return
		}
		if year < 0 {
			err = ErrYearParse
			return
		}
	} else {
		err = ErrYearParse
		return
	}
	// Create the year
	y, err = NewYear(year)
	return
}
