// Mysqltime contains types to simplify working with MySQL DATE, DATETIME, TIME, and YEAR values.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mysqltime

import (
	"errors"
)

// The string formats.
const (
	DateFormat     = "2006-01-02"
	DatetimeFormat = "2006-01-02 15:04:05"
	TimeFormat     = "15:04:05"
	YearFormat     = "2006"
)

// The zero strings.
const (
	ZeroDate     = "0000-00-00"
	ZeroDatetime = "0000-00-00 00:00:00"
	ZeroTime     = "00:00:00"
	ZeroYear     = "0000"
)

// Common errors.
var (
	ErrDateParse     = errors.New("Invalid Date string.")
	ErrDateRange     = errors.New("Invalid Date range.")
	ErrDateScan      = errors.New("Unable to convert value to a Date.")
	ErrDatetimeParse = errors.New("Invalid Datetime string.")
	ErrDatetimeRange = errors.New("Invalid Datetime range.")
	ErrDatetimeScan  = errors.New("Unable to convert value to a Datetime.")
	ErrTimeParse     = errors.New("Invalid Time string.")
	ErrTimeRange     = errors.New("Invalid Time range.")
	ErrTimeScan      = errors.New("Unable to convert value to a Time.")
	ErrYearParse     = errors.New("Invalid Year string.")
	ErrYearRange     = errors.New("Invalid Year range.")
	ErrYearScan      = errors.New("Unable to convert value to a Year.")
)
