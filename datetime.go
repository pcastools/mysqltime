// Datetime contains functions and types to simplify working with MySQL DATETIMEs. Datetimes are location independent.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mysqltime

import (
	"database/sql/driver"
	"strconv"
	"strings"
	"time"
)

// Datetime represents a datetime. Datetimes are location independent.
type Datetime time.Time

/////////////////////////////////////////////////////////////////////////
// Datetime functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the datetime
func (t Datetime) String() string {
	if t.IsZero() {
		return ZeroDatetime
	}
	return time.Time(t).Format(DatetimeFormat)
}

// Value returns the datetime as a driver.Value, satisfying the driver.Valuer interface.
func (t Datetime) Value() (driver.Value, error) {
	return t.String(), nil
}

// Scan assigns a value to this Datetime from a database driver. The src value should be one of the types defined in the sql.Scanner interface.
func (t *Datetime) Scan(src interface{}) (err error) {
	switch val := src.(type) {
	case nil:
	case time.Time:
		*t, err = NewDatetime(val.Year(), val.Month(), val.Day(), val.Hour(), val.Minute(), val.Second())
	case string:
		*t, err = ParseDatetime(val)
	case []byte:
		*t, err = ParseDatetime(string(val))
	default:
		err = ErrDatetimeScan
	}
	return
}

// After returns true iff the datetime t is after u.
func (t Datetime) After(u Datetime) bool {
	y1, m1, d1 := t.Date()
	y2, m2, d2 := u.Date()
	if y1 > y2 {
		return true
	} else if y1 == y2 {
		if m1 > m2 {
			return true
		} else if m1 == m2 {
			if d1 > d2 {
				return true
			} else if d1 == d2 {
				h1, min1, s1 := t.Clock()
				h2, min2, s2 := u.Clock()
				if h1 > h2 {
					return true
				} else if h1 == h2 {
					if min1 > min2 {
						return true
					} else if min1 == min2 {
						return s1 > s2
					}
				}
			}
		}
	}
	return false
}

// Before returns true iff the datetime t is before u.
func (t Datetime) Before(u Datetime) bool {
	y1, m1, d1 := t.Date()
	y2, m2, d2 := u.Date()
	if y1 < y2 {
		return true
	} else if y1 == y2 {
		if m1 < m2 {
			return true
		} else if m1 == m2 {
			if d1 < d2 {
				return true
			} else if d1 == d2 {
				h1, min1, s1 := t.Clock()
				h2, min2, s2 := u.Clock()
				if h1 < h2 {
					return true
				} else if h1 == h2 {
					if min1 < min2 {
						return true
					} else if min1 == min2 {
						return s1 < s2
					}
				}
			}
		}
	}
	return false
}

// Equal returns true iff the datetime t is equal to u.
func (t Datetime) Equal(u Datetime) bool {
	y1, m1, d1 := t.Date()
	y2, m2, d2 := u.Date()
	if y1 != y2 || m1 != m2 || d1 != d2 {
		return false
	}
	h1, min1, s1 := t.Clock()
	h2, min2, s2 := u.Clock()
	return h1 == h2 && min1 == min2 && s1 == s2
}

// Year returns the year.
func (t Datetime) Year() int {
	if t.IsZero() {
		return 0
	}
	return time.Time(t).Year()
}

// Month returns the month.
func (t Datetime) Month() time.Month {
	if t.IsZero() {
		return time.Month(0)
	}
	return time.Time(t).Month()
}

// Day returns the day.
func (t Datetime) Day() int {
	if t.IsZero() {
		return 0
	}
	return time.Time(t).Day()
}

// Hour returns the hour.
func (t Datetime) Hour() int {
	if t.IsZero() {
		return 0
	}
	return time.Time(t).Hour()
}

// Minute returns the minute.
func (t Datetime) Minute() int {
	if t.IsZero() {
		return 0
	}
	return time.Time(t).Minute()
}

// Second returns the second.
func (t Datetime) Second() int {
	if t.IsZero() {
		return 0
	}
	return time.Time(t).Second()
}

// Weekday returns the weekday.
func (t Datetime) Weekday() time.Weekday {
	if t.IsZero() {
		return time.Weekday(0)
	}
	return time.Time(t).Weekday()
}

// YearDay returns the day of the year in the range [1,365] for non-leap years, and [1,366] in leap years. Returns 0 if this is the zero date.
func (t Datetime) YearDay() int {
	if t.IsZero() {
		return 0
	}
	return time.Time(t).YearDay()
}

// Date returns the year, month, and day of the datetime.
func (t Datetime) Date() (int, time.Month, int) {
	if t.IsZero() {
		return 0, time.Month(0), 0
	}
	return time.Time(t).Date()
}

// Clock returns the hour, minute, and second of the datetime.
func (t Datetime) Clock() (int, int, int) {
	if t.IsZero() {
		return 0, 0, 0
	}
	return time.Time(t).Clock()
}

// IsZero return true if the datetime is outside the supported range.
func (t Datetime) IsZero() bool {
	year := time.Time(t).Year()
	return year < 1000 || year > 9999
}

// Time converts the Datetime to time.Time using the given location.
func (t Datetime) Time(loc *time.Location) (tt time.Time) {
	if t.IsZero() {
		return
	}
	year, month, day := t.Date()
	hour, min, sec := t.Clock()
	tt = time.Date(year, month, day, hour, min, sec, 0, loc)
	return
}

// Local converts the Datetime to time.Time using Local location.
func (t Datetime) Local() time.Time {
	return t.Time(time.Local)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// TimeToDatetime creates a new datetime using the given time.Time. The year, month, etc. are read from the time.Time using its defined location.
func TimeToDatetime(t time.Time) Datetime {
	tt, err := NewDatetime(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
	if err != nil {
		panic(err)
	}
	return tt
}

// NewDatetime creates a new datetime using the given data. The year must lie in the range 1000 to 9999, inclusive, or all arguments must be 0.
func NewDatetime(year int, month time.Month, day int, hour int, min int, sec int) (t Datetime, err error) {
	// Get the zero datetime out of the way
	if year == 0 && month == 0 && day == 0 && hour == 0 && min == 0 && sec == 0 {
		return
	}
	// Sanity check on the year
	if year < 1000 || year > 9999 {
		err = ErrDatetimeRange
		return
	}
	// Sanity check on the month
	if month <= 0 || month > 12 {
		err = ErrDatetimeRange
		return
	}
	// Sanity check on the day
	switch month {
	case time.February:
		if day <= 0 || day > 29 {
			err = ErrDatetimeRange
			return
		}
	case time.April, time.June, time.September, time.November:
		if day <= 0 || day > 30 {
			err = ErrDatetimeRange
			return
		}
	default:
		if day <= 0 || day > 31 {
			err = ErrDatetimeRange
			return
		}
	}
	// Sanity check on the hour
	if hour < 0 || hour >= 24 {
		err = ErrDatetimeRange
		return
	}
	// Sanity check on the minute
	if min < 0 || min >= 60 {
		err = ErrDatetimeRange
		return
	}
	// Sanity check on the second
	if sec < 0 || sec >= 60 {
		err = ErrDatetimeRange
		return
	}
	// Create the datetime
	t = Datetime(time.Date(year, month, day, hour, min, sec, 0, time.UTC))
	return
}

// ParseDatetime converts the string format "YYYY-MM-DD hh:mm:ss" to a Datetime. Leading and trailing spaces are ignored.
func ParseDatetime(value string) (t Datetime, err error) {
	// Strip off any white space
	value = strings.TrimSpace(value)
	// Special-case for a zero datetime
	if value == ZeroDatetime {
		return
	}
	// Sanity check on the structure of the string
	if len(value) != 19 || value[4] != '-' || value[7] != '-' || value[10] != ' ' || value[13] != ':' || value[16] != ':' {
		err = ErrDatetimeParse
		return
	}
	// Parse the year
	var year int
	if year, err = strconv.Atoi(value[:4]); err != nil {
		err = ErrDatetimeParse
		return
	}
	// Parse the month
	var month int
	if month, err = strconv.Atoi(value[5:7]); err != nil {
		err = ErrDatetimeParse
		return
	}
	// Parse the day
	var day int
	if day, err = strconv.Atoi(value[8:10]); err != nil {
		err = ErrDatetimeParse
		return
	}
	// Parse the hour
	var hour int
	if hour, err = strconv.Atoi(value[11:13]); err != nil {
		err = ErrDatetimeParse
		return
	}
	// Parse the minute
	var min int
	if min, err = strconv.Atoi(value[14:16]); err != nil {
		err = ErrDatetimeParse
		return
	}
	// Parse the second
	var sec int
	if sec, err = strconv.Atoi(value[17:]); err != nil {
		err = ErrDatetimeParse
		return
	}
	// Create the datetime
	t, err = NewDatetime(year, time.Month(month), day, hour, min, sec)
	return
}
