// Date contains gunctions and types to simplify working with MySQL DATEs. Dates are location independent.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mysqltime

import (
	"database/sql/driver"
	"strconv"
	"strings"
	"time"
)

// Date represents a date. Dates are location independent.
type Date time.Time

/////////////////////////////////////////////////////////////////////////
// Date functions
/////////////////////////////////////////////////////////////////////////

// String returns the Date in the format "YYYY-MM-DD".
func (d Date) String() string {
	if d.IsZero() {
		return ZeroDate
	}
	return time.Time(d).Format(DateFormat)
}

// Value returns the Date as a driver.Value, satisfying the driver.Valuer interface.
func (d Date) Value() (driver.Value, error) {
	return d.String(), nil
}

// Scan assigns a value to this Date from a database driver. The src value should be one of the types defined in the sql.Scanner interface.
func (d *Date) Scan(src interface{}) (err error) {
	switch val := src.(type) {
	case nil:
	case time.Time:
		*d, err = NewDate(val.Year(), val.Month(), val.Day())
	case string:
		*d, err = ParseDate(val)
	case []byte:
		*d, err = ParseDate(string(val))
	default:
		err = ErrDateScan
	}
	return
}

// After returns true iff the date d is after u.
func (d Date) After(u Date) bool {
	y1, m1, d1 := d.Date()
	y2, m2, d2 := u.Date()
	if y1 > y2 {
		return true
	} else if y1 == y2 {
		if m1 > m2 {
			return true
		} else if m1 == m2 {
			return d1 > d2
		}
	}
	return false
}

// Before returns true iff the date d is before u.
func (d Date) Before(u Date) bool {
	y1, m1, d1 := d.Date()
	y2, m2, d2 := u.Date()
	if y1 < y2 {
		return true
	} else if y1 == y2 {
		if m1 < m2 {
			return true
		} else if m1 == m2 {
			return d1 < d2
		}
	}
	return false
}

// Equal returns true iff the date d is equal to u.
func (d Date) Equal(u Date) bool {
	y1, m1, d1 := d.Date()
	y2, m2, d2 := u.Date()
	return y1 == y2 && m1 == m2 && d1 == d2
}

// Year returns the year.
func (d Date) Year() int {
	if d.IsZero() {
		return 0
	}
	return time.Time(d).Year()
}

// Month returns the month.
func (d Date) Month() time.Month {
	if d.IsZero() {
		return time.Month(0)
	}
	return time.Time(d).Month()
}

// Day returns the day.
func (d Date) Day() int {
	if d.IsZero() {
		return 0
	}
	return time.Time(d).Day()
}

// Weekday returns the weekday.
func (d Date) Weekday() time.Weekday {
	if d.IsZero() {
		return time.Weekday(0)
	}
	return time.Time(d).Weekday()
}

// YearDay returns the day of the year in the range [1,365] for non-leap years, and [1,366] in leap years. Returns 0 if this is the zero date.
func (d Date) YearDay() int {
	if d.IsZero() {
		return 0
	}
	return time.Time(d).YearDay()
}

// Date returns the components of the date.
func (d Date) Date() (int, time.Month, int) {
	if d.IsZero() {
		return 0, time.Month(0), 0
	}
	return time.Time(d).Date()
}

// IsZero return true if the date is outside the supported range.
func (d Date) IsZero() bool {
	year := time.Time(d).Year()
	return year < 1000 || year > 9999
}

// Time converts the Date to time.Time using the given location.
func (d Date) Time(loc *time.Location) (t time.Time) {
	if d.IsZero() {
		return
	}
	year, month, day := d.Date()
	t = time.Date(year, month, day, 0, 0, 0, 0, loc)
	return
}

// Local converts the Date to time.Time using Local location
func (d Date) Local() time.Time {
	return d.Time(time.Local)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewDate creates a new date using the given year, month, and day. The year must lie in the range 1000 to 9999, inclusive, or all arguments must be 0.
func NewDate(year int, month time.Month, day int) (d Date, err error) {
	// Get the zero date out of the way
	if year == 0 && month == 0 && day == 0 {
		return
	}
	// Sanity check on the year
	if year < 1000 || year > 9999 {
		err = ErrDateRange
		return
	}
	// Sanity check on the month
	if month <= 0 || month > 12 {
		err = ErrDateRange
		return
	}
	// Sanity check on the day
	switch month {
	case time.February:
		if day <= 0 || day > 29 {
			err = ErrDateRange
			return
		}
	case time.April, time.June, time.September, time.November:
		if day <= 0 || day > 30 {
			err = ErrDateRange
			return
		}
	default:
		if day <= 0 || day > 31 {
			err = ErrDateRange
			return
		}
	}
	// Create the date
	d = Date(time.Date(year, month, day, 0, 0, 0, 0, time.UTC))
	return
}

// ParseDate converts the string format "YYYY-MM-DD" to a Date. Leading and trailing spaces are ignored.
func ParseDate(value string) (d Date, err error) {
	// Strip off any white space
	value = strings.TrimSpace(value)
	// Special-case for a zero date
	if value == ZeroDate {
		return
	}
	// Sanity check on the structure of the string
	if len(value) != 10 || value[4] != '-' || value[7] != '-' {
		err = ErrDateParse
		return
	}
	// Parse the year
	var year int
	if year, err = strconv.Atoi(value[:4]); err != nil {
		err = ErrDateParse
		return
	}
	// Parse the month
	var month int
	if month, err = strconv.Atoi(value[5:7]); err != nil {
		err = ErrDateParse
		return
	}
	// Parse the day
	var day int
	if day, err = strconv.Atoi(value[8:]); err != nil {
		err = ErrDateParse
		return
	}
	// Create the date
	d, err = NewDate(year, time.Month(month), day)
	return
}
