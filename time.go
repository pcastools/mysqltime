// Time contains functions and types to simplify working with MySQL TIMEs. The time represents a duration of time, down to the resolution of a second.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mysqltime

import (
	"database/sql/driver"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// Time represents a duration of time.
type Time time.Duration

/////////////////////////////////////////////////////////////////////////
// Time functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the time.
func (t Time) String() string {
	hour, min, sec := t.Clock()
	return fmt.Sprintf("%d:%02d:%02d", hour, min, sec)
}

// Value returns the time as a driver.Value, satisfying the driver.Valuer interface.
func (t Time) Value() (driver.Value, error) {
	return t.String(), nil
}

// Scan assigns a value to this Time from a database driver. The src value should be one of the types defined in the sql.Scanner interface.
func (t *Time) Scan(src interface{}) (err error) {
	switch val := src.(type) {
	case nil:
	case int64:
		*t, err = DurationToTime(time.Duration(val))
	case time.Time:
		*t, err = NewTime(val.Hour(), val.Minute(), val.Second())
	case string:
		*t, err = ParseTime(val)
	case []byte:
		*t, err = ParseTime(string(val))
	default:
		err = ErrTimeScan
	}
	return
}

// After returns true iff the time t is after u.
func (t Time) After(u Time) bool {
	h1, m1, s1 := t.Clock()
	h2, m2, s2 := u.Clock()
	if h1 > h2 {
		return true
	} else if h1 == h2 {
		if m1 > m2 {
			return true
		} else if m1 == m2 {
			return s1 > s2
		}
	}
	return false
}

// Before returns true iff the time t is before u.
func (t Time) Before(u Time) bool {
	h1, m1, s1 := t.Clock()
	h2, m2, s2 := u.Clock()
	if h1 < h2 {
		return true
	} else if h1 == h2 {
		if m1 < m2 {
			return true
		} else if m1 == m2 {
			return s1 < s2
		}
	}
	return false
}

// Equal returns true iff the time t is equal to u.
func (t Time) Equal(u Time) bool {
	h1, m1, s1 := t.Clock()
	h2, m2, s2 := u.Clock()
	return h1 == h2 && m1 == m2 && s1 == s2
}

// Hour returns the hour.
func (t Time) Hour() int {
	hour, _, _ := t.Clock()
	return hour
}

// Minute returns the minute.
func (t Time) Minute() int {
	_, min, _ := t.Clock()
	return min
}

// Second returns the second.
func (t Time) Second() int {
	_, _, sec := t.Clock()
	return sec
}

// Clock returns the hour, minute, and second of the time.
func (t Time) Clock() (int, int, int) {
	n := int64(t)
	sign := 1
	if n < 0 {
		sign = -1
		n = -n
	}
	n /= 1e9
	sec := int(n % 60)
	n /= 60
	min := int(n % 60)
	hour := int(n/60) * sign
	if hour < -838 {
		return -838, 59, 59
	} else if hour > 838 {
		return 838, 59, 59
	}
	return hour, min, sec
}

// IsZero return true if the time is zero.
func (t Time) IsZero() bool {
	hour, min, sec := t.Clock()
	return hour == 0 && min == 0 && sec == 0
}

// Time converts the Time to time.Time using the given location.
func (t Time) Time(loc *time.Location) time.Time {
	hour, min, sec := t.Clock()
	return time.Date(0, time.January, 1, hour, min, sec, 0, loc)
}

// Local converts the Time to time.Time using Local location.
func (t Time) Local() time.Time {
	return t.Time(time.Local)
}

// Duration converts the Time to time.Duration.
func (t Time) Duration() time.Duration {
	hour, min, sec := t.Clock()
	return time.Duration(time.Hour*time.Duration(hour) + time.Minute*time.Duration(min) + time.Second*time.Duration(sec))
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewTime creates a new time using the given data. The number of hours must lie in the range -838 to 838, inclusive.
func NewTime(hour int, min int, sec int) (t Time, err error) {
	// Sanity check on the hour
	if hour < -838 || hour > 838 {
		err = ErrTimeRange
		return
	}
	// Sanity check on the minute
	if min < 0 || min >= 60 {
		err = ErrTimeRange
		return
	}
	// Sanity check on the second
	if sec < 0 || sec >= 60 {
		err = ErrTimeRange
		return
	}
	// Create the time
	t = Time(time.Hour*time.Duration(hour) + time.Minute*time.Duration(min) + time.Second*time.Duration(sec))
	return
}

// DurationToTime creates a new time from the given duration.
func DurationToTime(d time.Duration) (t Time, err error) {
	n := int64(d)
	sign := 1
	if n < 0 {
		sign = -1
		n = -n
	}
	n /= 1e9
	sec := int(n % 60)
	n /= 60
	min := int(n % 60)
	hour := int(n/60) * sign
	t, err = NewTime(hour, min, sec)
	return
}

// ParseTime converts the string format "[+-]h+:mm:ss" to a Time. Leading and trailing spaces are ignored.
func ParseTime(value string) (t Time, err error) {
	// Strip off any white space
	value = strings.TrimSpace(value)
	// Find the index of the first colon
	idx := strings.IndexRune(value, ':')
	if idx == -1 {
		// We interpret the value as the number of seconds
		var n int64
		if n, err = strconv.ParseInt(value, 10, 64); err != nil {
			err = ErrTimeParse
			return
		}
		// Convert this to hours, minutes, and seconds
		sec := int(n % 60)
		n /= 60
		min := int(n % 60)
		hour := int(n / 60)
		// Create the time and return
		t, err = NewTime(hour, min, sec)
		return
	}
	// Sanity check on the index
	if idx == len(value)-1 {
		err = ErrTimeParse
		return
	}
	// The value up to the first colon is always the number of hours
	var hour int
	if hour, err = strconv.Atoi(value[:idx]); err != nil {
		err = ErrTimeParse
		return
	}
	// What remains should be either "mm:ss" or "mm"
	var min, sec int
	value = value[idx+1:]
	N := len(value)
	if N == 2 {
		if min, err = strconv.Atoi(value); err != nil {
			err = ErrTimeParse
			return
		}
	} else if N == 5 && value[2] == ':' {
		if min, err = strconv.Atoi(value[:2]); err != nil {
			err = ErrTimeParse
			return
		}
		if sec, err = strconv.Atoi(value[3:]); err != nil {
			err = ErrTimeParse
			return
		}
	} else {
		err = ErrTimeParse
		return
	}
	// Create the time
	t, err = NewTime(hour, min, sec)
	return
}
